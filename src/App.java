public class App {
    public static void main(String[] args) throws Exception {
       int[] a1 = {1,2,3,4};
       char[] a2 = {'a','b','c'};

       //Ghi ra console
       System.out.println("các phần tử trong mảng a1");
       for (int i = 0; i < a1.length; i++) {
        System.out.println(a1[i]);
       }

       System.out.println("các phần tử trong mảng a2");
       for (int i = 0; i < a2.length; i++) {
        System.out.println(a2[i]);
       }

       //Ghi ra ký tự trong mảng a2
       System.out.println("mảng a2");
       System.out.println(a2); 
    }

}
